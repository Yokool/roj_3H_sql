package com.Roj.gui.consts;

import java.awt.*;

public class FontConsts
{

    private static final String FONT_NAME = "Sans-serif";

    public static final Font HEADER_FONT = new Font(FONT_NAME, Font.BOLD, 26);
    public static final Font SUBHEADER_FONT = new Font(FONT_NAME, Font.ITALIC, 20);

}

package com.Roj.gui.panels;

import com.Roj.gbconstr.GBConstr;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel
{
    private MainContentPanel main_content_panel = new MainContentPanel();

    public MainPanel()
    {
        comp_init();
    }

    private void comp_init()
    {
        setLayout(get_layout());

        GBConstr base_constr = new GBConstr()
                .set_weight_y(1.0)
                .set_fill(GridBagConstraints.BOTH);

        add(new JPanel(), new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );
        add(main_content_panel, new GBConstr(base_constr)
                .set_position(1, 0)
                .get_result()
        );
        add(new JPanel(), new GBConstr(base_constr)
                .set_position(2, 0)
                .get_result()
        );
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();

        gb_layout.columnWidths = new int[] {
                50,
                0,
                50
        };

        gb_layout.columnWeights = new double[] {
                0.0,
                15.0,
                0.0
        };

        return gb_layout;
    }



}

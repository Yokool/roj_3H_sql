package com.Roj.gui.panels;

import com.Roj.gbconstr.GBConstr;
import com.Roj.gui.consts.FontConsts;
import com.Roj.gui.inputcomponents.RegionPicker;
import com.Roj.gui.inputcomponents.SubregionPicker;
import com.Roj.gui.outcomps.SubsubRegionList;

import javax.swing.*;
import java.awt.*;

public class MainContentPanel extends JPanel
{

    private JLabel header = new JLabel("Výpis obcí - Roj");

    private SubsubRegionList subsubregion_list = new SubsubRegionList();
    private SubregionPicker subregion_picker = new SubregionPicker(subsubregion_list);
    private RegionPicker region_picker = new RegionPicker(subregion_picker);

    public MainContentPanel()
    {
        comp_init();
        children_init();
        add_children();
    }

    private void children_init()
    {
        header.setFont(FontConsts.HEADER_FONT);
    }

    private void add_children()
    {
        GBConstr base_constr = new GBConstr()
                .set_weight_x(1.0)
                .set_fill(GridBagConstraints.BOTH);

        add(header, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        add(region_picker, new GBConstr(base_constr)
                .set_position(0, 1)
                .get_result()
        );

        add(subregion_picker, new GBConstr(base_constr)
                .set_position(0, 2)
                .get_result()
        );

        add(subsubregion_list, new GBConstr(base_constr)
                .set_position(0, 3)
                .get_result()
        );

        add(new JPanel(), new GBConstr(base_constr)
                .set_position(0, 4)
                .set_weight_y(1.0)
                .get_result()
        );
    }

    private void comp_init()
    {
        setLayout(get_layout());
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();

        gb_layout.rowHeights = new int[] {
                40, // Header
                60, // Region picker
                60, // Subregion picker
                360, // Subsub list
        };

        return gb_layout;
    }

}

package com.Roj.gui;

import com.Roj.gbconstr.GBConstr;
import com.Roj.gui.panels.MainPanel;

import javax.swing.*;
import java.awt.*;

public class AppFrame extends JFrame
{
    private static final String APP_NAME = "Výpis obcí - Pavel Roj";

    private MainPanel main_panel = new MainPanel();

    public AppFrame()
    {
        frame_init();

        add(main_panel, new GBConstr()
                .set_position(0, 0)
                .max_expand()
                .get_result()
        );
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();
        return gb_layout;
    }

    private void frame_init()
    {
        setTitle(APP_NAME);
        setSize(1080, 720);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        setLayout(get_layout());
    }

}

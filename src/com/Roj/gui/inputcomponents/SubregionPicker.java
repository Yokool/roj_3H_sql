package com.Roj.gui.inputcomponents;

import com.Roj.database.DBConn;
import com.Roj.gui.outcomps.SubsubRegionList;
import com.Roj.swinghelper.SwingHelper;

import javax.swing.*;
import java.util.ArrayList;

public class SubregionPicker extends PickerBase
{

    private SubsubRegionList sub_list;

    public SubregionPicker(SubsubRegionList sub_list)
    {
        super("Vyberte okres:");
        this.sub_list = sub_list;
    }

    public void update_selection_from_region(String region)
    {
        ArrayList<String> subregions = DBConn.get_subregions(region);
        picker.removeAllItems();
        SwingHelper.combo_box_add_values(picker, subregions);

    }

    @Override
    protected void on_picker_change(String new_item)
    {
        sub_list.update_list(new_item);
    }
}

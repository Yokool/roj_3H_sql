package com.Roj.gui.inputcomponents;

import com.Roj.gbconstr.GBConstr;
import com.Roj.gui.consts.FontConsts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

public class PickerBase extends JPanel
{

    private JLabel picker_header = new JLabel();
    protected JComboBox<String> picker = new JComboBox<>();

    protected PickerBase(String picker_header_text)
    {
        picker_header.setText(picker_header_text);
        picker_header.setFont(FontConsts.SUBHEADER_FONT);

        comp_init();
        children_init();
        picker_init();

        add_children();
    }

    private void comp_init()
    {
        setLayout(get_layout());
    }

    private void children_init()
    {
        picker.addItemListener(this::picker_click_func);
    }

    private void picker_click_func(ItemEvent event)
    {
        if(event.getStateChange() == ItemEvent.DESELECTED)
        {
            return;
        }

        String new_item = (String) event.getItem();
        on_picker_change(new_item);
    }

    protected void on_picker_change(String new_item)
    {

    }

    protected void picker_init()
    {
    }

    private void add_children()
    {
        GBConstr base_constr = new GBConstr()
                .set_weight_x(1.0)
                .set_fill(GridBagConstraints.BOTH);

        add(picker_header, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        add(picker, new GBConstr(base_constr)
                .set_position(0, 1)
                .get_result()
        );
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout layout = new GridBagLayout();

        layout.rowHeights = new int[] {
                40,
                20
        };

        return layout;
    }

}

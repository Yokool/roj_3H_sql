package com.Roj.gui.inputcomponents;

import com.Roj.database.DBConn;
import com.Roj.swinghelper.SwingHelper;

public class RegionPicker extends PickerBase
{

    private SubregionPicker subregion_picker;

    public RegionPicker(SubregionPicker subregion_picker)
    {
        super("Vyberte kraj");
        this.subregion_picker = subregion_picker;

        // Manually update after subregion_picker initialization
        on_picker_change((String)picker.getSelectedItem());
    }

    @Override
    protected void picker_init()
    {
        SwingHelper.combo_box_add_values(picker, DBConn.get_regions());
    }

    @Override
    protected void on_picker_change(String new_item)
    {
        // This can get called while adding items before subregion_picker
        // is initialized.
        if(subregion_picker == null)
        {
            return;
        }

        subregion_picker.update_selection_from_region(new_item);
    }
}

package com.Roj.gui.outcomps;

import com.Roj.database.DBConn;
import com.Roj.gbconstr.GBConstr;
import com.Roj.swinghelper.SwingHelper;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class SubsubRegionList extends JPanel
{

    private JLabel header = new JLabel("Obce:");
    private List subsubregion_list = new List();

    public SubsubRegionList()
    {
        comp_init();
        children_init();
        add_children();

        //update_list("Šumperk");
    }

    private void comp_init()
    {
        setLayout(get_layout());
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gridbag_layout = new GridBagLayout();

        gridbag_layout.rowHeights = new int[] {
                40,
                320
        };

        return gridbag_layout;
    }

    private void children_init()
    {

    }

    private void add_children()
    {
        GBConstr base_constr = new GBConstr()
                .set_weight_x(1.0)
                .set_fill(GridBagConstraints.BOTH);

        add(header, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        add(subsubregion_list, new GBConstr(base_constr)
                .set_position(0, 1)
                .get_result()
        );
    }

    public void update_list(String subregion_name)
    {
        subsubregion_list.removeAll();
        ArrayList<String> subsubregions = DBConn.get_subsubregions(subregion_name);
        SwingHelper.awt_list_add_values(subsubregion_list, subsubregions);
    }

}

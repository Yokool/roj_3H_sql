package com.Roj.gbconstr;

import java.awt.*;

public class GBConstr
{

    private int grid_x = 0;
    private int grid_y = 0;
    private double weight_x = 0.0;
    private double weight_y = 0.0;
    private int fill = GridBagConstraints.NONE;

    public GBConstr set_weight_x(double weight_x)
    {
        this.weight_x = weight_x;
        return this;
    }

    public GBConstr set_weight_y(double weight_y)
    {
        this.weight_y = weight_y;
        return this;
    }

    public GBConstr set_weights(double weight_x, double weight_y)
    {
        set_weight_x(weight_x);
        return set_weight_y(weight_y);
    }

    public GBConstr set_position(int grid_x, int grid_y)
    {
        set_grid_x(grid_x);
        return set_grid_y(grid_y);
    }

    public GBConstr set_grid_x(int grid_x)
    {
        this.grid_x = grid_x;
        return this;
    }

    public GBConstr set_grid_y(int grid_y)
    {
        this.grid_y = grid_y;
        return this;
    }

    public GBConstr set_fill(int fill)
    {
        this.fill = fill;
        return this;
    }

    public GBConstr max_expand()
    {
        set_weights(1.0, 1.0);
        set_fill(GridBagConstraints.BOTH);
        return this;
    }

    public GBConstr()
    {

    }

    public GBConstr(GBConstr copy)
    {
        this.grid_x = copy.grid_x;
        this.grid_y = copy.grid_y;
        this.weight_x = copy.weight_x;
        this.weight_y = copy.weight_y;
        this.fill = copy.fill;
    }

    public GridBagConstraints get_result()
    {
        GridBagConstraints result = new GridBagConstraints();
        result.gridx = grid_x;
        result.gridy = grid_y;
        result.weightx = weight_x;
        result.weighty = weight_y;
        result.fill = fill;
        return result;
    }

}

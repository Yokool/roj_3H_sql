package com.Roj.swinghelper;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class SwingHelper
{

    public static <T> void combo_box_add_values(JComboBox<T> combo_box, List<T> values)
    {
        for(T val : values)
        {
            combo_box.addItem(val);
        }
    }

    public static void awt_list_add_values(java.awt.List list, List<String> values)
    {
        for(String val : values)
        {
            list.add(val);
        }
    }

}
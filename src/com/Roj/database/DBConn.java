package com.Roj.database;

import java.sql.*;
import java.util.ArrayList;

public class DBConn
{

    private static Connection db_connection;

    public static Connection get_db_connection()
    {
        return db_connection;
    }

    public static ResultSet execute_query(String query_str)
    {
        try {
            PreparedStatement sql_statement = get_db_connection().prepareStatement(query_str);
            return execute_query(sql_statement);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong while trying to create the statement:\n" + query_str);
        }
    }

    public static ResultSet execute_query(PreparedStatement query_sql)
    {
        try {
            return query_sql.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong while trying to execute an sql statement " + query_sql);
        }
    }

    public static ArrayList<String> get_subregions(String region)
    {
        String region_id = get_region_post_id(region);
        String sql = "SELECT * FROM obce WHERE KrajID=" + region_id + " GROUP BY OkresID";
        return collect_results(execute_query(sql), "Okres");
    }

    public static String get_subregion_id(String subregion)
    {
        String sql = "SELECT * FROM obce WHERE Okres='" + subregion + "' GROUP BY OkresID";
        ResultSet result = execute_query(sql);

        try {
            if(!result.next())
            {
                throw new RuntimeException("No result while trying to get the subregion id of subregion " + subregion);
            }

            return String.valueOf(result.getInt("OkresID"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong while trying to get the subregion id of subregion " + subregion);
        }
    }

    public static ArrayList<String> get_subsubregions(String subregion)
    {
        String subregion_id = get_subregion_id(subregion);
        String sql = "SELECT * FROM obce WHERE OkresID=" + subregion_id + " GROUP BY ObecID";
        ResultSet query_result = execute_query(sql);
        return collect_results(query_result, "Obec");
    }

    public static ArrayList<String> collect_results(ResultSet result_set, String column_label)
    {
        ArrayList<String> result_str = new ArrayList<>();

        try
        {
            while(result_set.next())
            {
                result_str.add(result_set.getString(column_label));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong while trying to get the region list.");
        }

        return result_str;
    }

    public static String get_region_post_id(String region)
    {
        String sql = "SELECT * FROM kraj WHERE Kraj='" + region + "'";
        ResultSet result_set = execute_query(sql);

        try {
            if(!result_set.next())
            {
                throw new RuntimeException("No region found under region name " + region);
            }

            return String.valueOf(result_set.getInt("PostaID"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong while trying to get the region id of region " + region);
        }
    }

    public static ArrayList<String> get_regions()
    {
        String region_selection_sql = "SELECT * FROM kraj";
        ResultSet results = execute_query(region_selection_sql);
        return collect_results(results, "Kraj");
    }

    private static void connect_to_db()
    {
        try
        {
            db_connection = DriverManager.getConnection(DatabaseConst.MARIADB_IP);
            execute_query("USE seznamobci");
        }
        catch (SQLException exception)
        {
            System.out.println(exception.getMessage());
            throw new RuntimeException("Something went wrong while trying to connect to the main database.");
        }
    }

    static {
        connect_to_db();
    }

}
